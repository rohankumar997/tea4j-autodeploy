#!/bin/sh

# Allows for restarting the last build of any repository. This enables us to create simple cronjobs for pipelines which
# is still not supported by woodpecker itself

# Install woodpecker command line interface
cd ~ || (echo 'Failed to move to home directory' && exit 1)
curl -L https://github.com/woodpecker-ci/woodpecker/releases/download/v0.15.0/woodpecker-cli_linux_amd64.tar.gz | tar xz
chmod +x woodpecker-cli
cp woodpecker-cli /usr/bin

# Defining woodpecker credentials and mandatory settings
export WOODPECKER_TOKEN=# Enter your own token
export WOODPECKER_SERVER=https://ci.codeberg.org
export WOODPECKER_LOG_LEVEL=disabled

# Defining target repository and branch
REPOSITORY=gitnex/tea4j-autodeploy
BRANCH=main

LAST_BUILD_NUMBER=$(woodpecker-cli build last --format '{{ .Number }}' --branch $BRANCH $REPOSITORY)
woodpecker-cli build start $REPOSITORY "$LAST_BUILD_NUMBER"