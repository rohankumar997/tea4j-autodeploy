package org.gitnex.tea4j.v2.apis;

import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.GeneralAPISettings;
import org.gitnex.tea4j.v2.models.GeneralAttachmentSettings;
import org.gitnex.tea4j.v2.models.GeneralRepoSettings;
import org.gitnex.tea4j.v2.models.GeneralUISettings;
import retrofit2.Call;
import retrofit2.http.*;

public interface SettingsApi {
  /**
   * Get instance&#x27;s global settings for api
   *
   * @return Call&lt;GeneralAPISettings&gt;
   */
  @GET("settings/api")
  Call<GeneralAPISettings> getGeneralAPISettings();

  /**
   * Get instance&#x27;s global settings for Attachment
   *
   * @return Call&lt;GeneralAttachmentSettings&gt;
   */
  @GET("settings/attachment")
  Call<GeneralAttachmentSettings> getGeneralAttachmentSettings();

  /**
   * Get instance&#x27;s global settings for repositories
   *
   * @return Call&lt;GeneralRepoSettings&gt;
   */
  @GET("settings/repository")
  Call<GeneralRepoSettings> getGeneralRepositorySettings();

  /**
   * Get instance&#x27;s global settings for ui
   *
   * @return Call&lt;GeneralUISettings&gt;
   */
  @GET("settings/ui")
  Call<GeneralUISettings> getGeneralUISettings();
}
