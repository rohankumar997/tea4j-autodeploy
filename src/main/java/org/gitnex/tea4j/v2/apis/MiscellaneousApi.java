package org.gitnex.tea4j.v2.apis;

import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.MarkdownOption;
import org.gitnex.tea4j.v2.models.NodeInfo;
import org.gitnex.tea4j.v2.models.ServerVersion;
import retrofit2.Call;
import retrofit2.http.*;

public interface MiscellaneousApi {
  /**
   * Returns the nodeinfo of the Gitea application
   *
   * @return Call&lt;NodeInfo&gt;
   */
  @GET("nodeinfo")
  Call<NodeInfo> getNodeInfo();

  /**
   * Get default signing-key.gpg
   *
   * @return Call&lt;String&gt;
   */
  @GET("signing-key.gpg")
  Call<String> getSigningKey();

  /**
   * Returns the version of the Gitea application
   *
   * @return Call&lt;ServerVersion&gt;
   */
  @GET("version")
  Call<ServerVersion> getVersion();

  /**
   * Render a markdown document as HTML
   *
   * @param body (optional)
   * @return Call&lt;String&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("markdown")
  Call<String> renderMarkdown(@retrofit2.http.Body MarkdownOption body);

  /**
   * Render raw markdown as HTML
   *
   * @param body Request body to render (required)
   * @return Call&lt;String&gt;
   */
  @Headers({"Content-Type:text/plain"})
  @POST("markdown/raw")
  Call<String> renderMarkdownRaw(@retrofit2.http.Body String body);
}
