package org.gitnex.tea4j.v2.apis;

import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.ActivityPub;
import retrofit2.Call;
import retrofit2.http.*;

public interface ActivitypubApi {
  /**
   * Returns the Person actor for a user
   *
   * @param username username of the user (required)
   * @return Call&lt;ActivityPub&gt;
   */
  @GET("activitypub/user/{username}")
  Call<ActivityPub> activitypubPerson(@retrofit2.http.Path("username") String username);

  /**
   * Send to the inbox
   *
   * @param username username of the user (required)
   * @return Call&lt;Void&gt;
   */
  @POST("activitypub/user/{username}/inbox")
  Call<Void> activitypubPersonInbox(@retrofit2.http.Path("username") String username);
}
