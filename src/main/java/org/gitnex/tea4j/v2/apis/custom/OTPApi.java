package org.gitnex.tea4j.v2.apis.custom;

import java.util.List;
import org.gitnex.tea4j.v2.models.AccessToken;
import org.gitnex.tea4j.v2.models.CreateAccessTokenOption;
import org.gitnex.tea4j.v2.models.ServerVersion;
import retrofit2.Call;
import retrofit2.http.*;

public interface OTPApi {

  @GET("version")
  Call<ServerVersion> getVersion(@Header("X-Gitea-OTP") int otp);

  @GET("users/{username}/tokens")
  Call<List<AccessToken>> userGetTokens(
      @Header("X-Gitea-OTP") int otp,
      @Path("username") String username,
      @Query("page") Integer page,
      @Query("limit") Integer limit);

  @DELETE("users/{username}/tokens/{token}")
  Call<Void> userDeleteAccessToken(
      @Header("X-Gitea-OTP") int otp,
      @Path("username") String username,
      @Path("token") String token);

  @Headers({"Content-Type:application/json"})
  @POST("users/{username}/tokens")
  Call<AccessToken> userCreateToken(
      @Header("X-Gitea-OTP") int otp,
      @Path("username") String username,
      @Body CreateAccessTokenOption body);
}
