# CommitStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**context** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**creator** | [**User**](User.md) |  |  [optional]
**description** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**status** | **String** |  |  [optional]
**targetUrl** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**url** | **String** |  |  [optional]
