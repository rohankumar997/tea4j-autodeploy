# StopWatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created** | [**Date**](Date.md) |  |  [optional]
**duration** | **String** |  |  [optional]
**issueIndex** | **Long** |  |  [optional]
**issueTitle** | **String** |  |  [optional]
**repoName** | **String** |  |  [optional]
**repoOwnerName** | **String** |  |  [optional]
**seconds** | **Long** |  |  [optional]
