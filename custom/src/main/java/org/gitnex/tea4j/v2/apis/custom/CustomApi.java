package org.gitnex.tea4j.v2.apis.custom;

import org.gitnex.tea4j.v2.models.*;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface CustomApi {

  @GET("repos/{owner}/{repo}/contents/{filepath}")
  Call<List<ContentsResponse>> repoGetContentsList(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path(value = "filepath", encoded = false) String filepath,
      @Query("ref") String ref);

  /**
   * Remove a reaction from a comment of an issue
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param id id of the comment to edit (required)
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(
      method = "DELETE",
      path = "repos/{owner}/{repo}/issues/comments/{id}/reactions",
      hasBody = true)
  Call<Void> issueDeleteCommentReactionWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("id") Long id,
      @Body EditReactionOption body);

  /**
   * Remove a reaction from an issue
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the issue (required)
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "repos/{owner}/{repo}/issues/{index}/reactions", hasBody = true)
  Call<Void> issueDeleteIssueReactionWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("index") Long index,
      @Body EditReactionOption body);

  /**
   * Delete a file in a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath path of the file to delete (required)
   * @param body (required)
   * @return Call<FileDeleteResponse>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "repos/{owner}/{repo}/contents/{filepath}", hasBody = true)
  Call<FileDeleteResponse> repoDeleteFileWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("filepath") String filepath,
      @Body DeleteFileOptions body);

  /**
   * Cancel review requests for a pull request
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param index index of the pull request (required)
   * @param body (required)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(
      method = "DELETE",
      path = "repos/{owner}/{repo}/pulls/{index}/requested_reviewers",
      hasBody = true)
  Call<Void> repoDeletePullReviewRequestsWithBody(
      @Path("owner") String owner,
      @Path("repo") String repo,
      @Path("index") Long index,
      @Body PullReviewRequestOptions body);

  /**
   * Delete email addresses
   *
   * @param body (optional)
   * @return Call<Void>;
   */
  @Headers({"Content-Type:application/json"})
  @HTTP(method = "DELETE", path = "user/emails", hasBody = true)
  Call<Void> userDeleteEmailWithBody(@Body DeleteEmailOption body);

    /**
   * Get a file from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath filepath of the file to get (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch (usually
   *     master) (optional)
   * @return Call<ResponseBody>;
   */
  @GET("repos/{owner}/{repo}/raw/{filepath}")
  Call<ResponseBody> repoGetRawFile(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);

  /**
   * Get a file or it's LFS object from a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param filepath filepath of the file to get (required)
   * @param ref The name of the commit/branch/tag. Default the repository’s default branch (usually
   *     master) (optional)
   * @return Call<ResponseBody>;
   */
  @GET("repos/{owner}/{repo}/media/{filepath}")
  Call<ResponseBody> repoGetRawFileOrLFS(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Path("filepath") String filepath,
      @retrofit2.http.Query("ref") String ref);
}
